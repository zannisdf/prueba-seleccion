import bodyParser from 'body-parser'
import config from './src/config'
import connectToDB from './src/db'
import cors from 'cors'
import express from 'express'
import path from 'path'
import router from './src/routes'

connectToDB()
  .then(() => {
    console.log('connectedToDb')
  })
  .catch(e => console.log(e))

const app = express()
const port = process.env.PORT || config.PORT

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(router)

if (process.env.NODE_ENV === 'production') {
  // Serve react static files
  app.use(express.static(path.join(__dirname, 'frontend/build')))

  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'frontend/build', 'index.html'))
  })
}

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
})
