import 'dotenv/config'

export default {
  DB_URI: process.env.DB_URI,
  PORT: 8080,
}
