import mongoose from 'mongoose'
import config from '../config'

const connectToDB = () =>
  mongoose.connect(config.DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

export default connectToDB
