import mongoose from 'mongoose'

const { model, Schema } = mongoose

const schema = new Schema({
  books: [String],
  gender: String,
  house: String,
  image: String,
  name: String,
  pagerank: {
    title: String,
    rank: Number,
  },
  slug: String,
  titles: [String],
})

const Character = model('Character', schema)

export default Character
