import { getAllResources, getResource } from '../utils/db-resources'
import getAllCharsAndSaveToDB from '../services/got-client'

import Character from '../models/character'

const characterRoutes = router => {
  router.get('/characters/:id', getResource(Character, 'slug'))
  router.get('/characters', getAllResources(Character))
  router.post('/characters', getAllCharsAndSaveToDB)
}

export default characterRoutes
