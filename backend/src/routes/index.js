import express from 'express'
import characterRoutes from './characters'

const router = express.Router()

characterRoutes(router)

export default router
