import axios from 'axios'
import Character from '../models/character'

const { get } = axios
const API_URL = 'https://api.got.show'
const ENDPOINT = 'api/general/characters'

// In order to avoid duplicates, `getAllCharsAndSaveToDB` deletes
// all entries from DB and saves new ones on each call.
const getAllCharsAndSaveToDB = () => {
  get(`${API_URL}/${ENDPOINT}`)
    .then(handleSuccessfulRequest)
    .catch(err => console.error(err))
}

const parseCharsToSchemaObj = characters =>
  characters.map(
    ({ books, gender, house, image, name, pagerank, slug, titles }) => ({
      books,
      gender,
      house,
      image,
      name,
      pagerank,
      slug,
      titles,
    })
  )

const handleDBTransactionError = success => err => {
  if (err) {
    return console.error(err)
  }
  console.log(success)
}

const deletePreviousChars = () =>
  Character.deleteMany(
    {},
    handleDBTransactionError('Characters successfully deleted')
  )

const saveCharsToDB = collection =>
  Character.insertMany(
    collection,
    handleDBTransactionError('Characters saved to the database')
  )

const handleSuccessfulRequest = ({ data: { book } }) => {
  const chars = parseCharsToSchemaObj(book)
  deletePreviousChars().then(() => saveCharsToDB(chars))
}

export default getAllCharsAndSaveToDB
