export const getAllResources = Model => (req, res) => {
  const page = Math.abs(parseInt(req.query.page)) || 1
  const size = Math.abs(parseInt(req.query.size)) || 0

  Model.find({})
    .skip(size * (page - 1))
    .limit(size)
    .exec((err, docs) => {
      catchError(err, res)

      const length = docs.length
      const numberOfPages = size ? Math.ceil(length / size) : null
      const successfulResponse = {
        characters: docs,
        numberOfPages,
        numberOfResults: length,
        page,
        size,
      }

      sendResponse(successfulResponse, res)
    })
}

export const getResource = (Model, idKey = '_id') => (req, res) => {
  const { id } = req.params

  Model.findOne({ [idKey]: id }, (err, doc) => {
    catchError(err, res)
    sendResponse(doc, res)
  })
}

const catchError = (err, res) => {
  if (err) {
    res.status(500).json({
      error: true,
      errors: err,
    })
  }
}

const sendResponse = (docs, res) => {
  if (!docs) {
    res.status(404).json({
      error: true,
      errors: 'Resource not found',
    })
  } else {
    res.status(200).json(docs)
  }
}
