import React from 'react'

import { Route, Switch } from 'react-router-dom'
import View from './components/View/View'
import List from './components/List'

const App = () => (
  <Switch>
    <Route path="/view/:id" component={View} />
    <Route path="/list" component={List} />
    <Route render={() => <div>Not found</div>} />
  </Switch>
)

export default App
