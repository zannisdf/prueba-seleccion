import React from 'react'
import PropTypes from 'prop-types'

const features = [
  { label: 'Name', key: 'name', type: 'string' },
  { label: 'Image', key: 'image', type: 'image' },
  { label: 'House', key: 'house', type: 'string' },
  { label: 'Titles', key: 'titles', type: 'array' },
  { label: 'Gender', key: 'gender', type: 'string' },
  {
    label: 'Slug (also used as character ID in this app)',
    key: 'slug',
    type: 'string',
  },
  { label: 'Books', key: 'books', type: 'array' },
  { label: 'Page Rank from api.got.show', key: 'pagerank', type: 'object' },
]

const Character = ({ character }) => (
  <ul>
    {features.map(({ label, key, type }) => (
      <li key={key}>
        {`${label}: `}
        {type === 'string' ? (
          character[key]
        ) : type === 'image' ? (
          <img src={character[key]} alt={character.name} />
        ) : (
          Object.values(character[key]).join(', ')
        )}
      </li>
    ))}
  </ul>
)

Character.propTypes = {
  character: PropTypes.object.isRequired,
}

export default Character
