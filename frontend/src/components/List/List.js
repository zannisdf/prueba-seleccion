import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Character from '../Character'

const initialStatus = {
  isLoading: false,
  isRejected: false,
  isFullfilled: false,
}

const List = () => {
  const [characters, setCharacters] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPages] = useState(0)
  const [{ isLoading, isRejected, isFullfilled }, setStatus] = useState({
    ...initialStatus,
    isLoading: true,
  })
  const ENDPOINT = '/characters'

  useEffect(() => {
    setStatus({ ...initialStatus, isLoading: true })
    const source = axios.CancelToken.source()
    const SIZE = 10
    axios
      .get(`${ENDPOINT}?page=${currentPage}&size=${SIZE}`, {
        cancelToken: source.token,
      })
      .then(res => {
        console.log(res.data)
        setCharacters(res.data.characters)
        setStatus({ ...initialStatus, isFullfilled: true })
      })
      .catch(err => {
        console.log(err.message)
        setStatus({ ...initialStatus, isRejected: true })
      })
    return () => source.cancel()
  }, [currentPage])

  const handlePageChange = index => () => {
    setCurrentPage(page => page + index || 1)
  }

  if (isLoading) return <div>Loading...</div>
  if (isRejected)
    return <div>Uh oh, we couldn't find what you were looking for!</div>
  if (isFullfilled) {
    return (
      <div>
        <button type="button" onClick={handlePageChange(-1)}>
          Prev Page
        </button>
        <button type="button" onClick={handlePageChange(1)}>
          Next Page
        </button>
        {characters.map((character, i) => (
          <Character key={`character_${i}`} character={character} />
        ))}
      </div>
    )
  }
}

export default List
