import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Character from '../Character'

const initialStatus = {
  isLoading: false,
  isRejected: false,
  isFullfilled: false,
}

const View = ({ match }) => {
  const [character, setCharacter] = useState({})
  const [{ isLoading, isRejected, isFullfilled }, setStatus] = useState({
    ...initialStatus,
    isLoading: true,
  })

  useEffect(() => {
    setStatus({ ...initialStatus, isLoading: true })
    const source = axios.CancelToken.source()
    axios
      .get(`/characters/${match.params.id}`, { cancelToken: source.token })
      .then(char => {
        setCharacter(char.data)
        setStatus({ ...initialStatus, isFullfilled: true })
      })
      .catch(err => {
        console.log(err.message)
        setStatus({ ...initialStatus, isRejected: true })
      })
    return () => source.cancel()
  }, [match])

  if (isLoading) return <div>Loading...</div>
  if (isRejected)
    return <div>Uh oh, we couldn't find what you were looking for!</div>
  if (isFullfilled) {
    return (
      <div>
        <Character character={character} />
      </div>
    )
  }
}

export default View
